// /////////////////////////////////////////////////////////////////
// Generated by axel-plugin wizard
// /////////////////////////////////////////////////////////////////
/* (C) MyCompany */
/* Put a short description of your plugin here */
/* MyCompany-contact@mycompany.com-http://www.mycompany.com */
#pragma once

#include "animationToolsExport.h"
#include <axlCore/axlPoint.h>
#include <axlCore/axlLine.h>
#include <axlCore/axlAbstractProcess.h>

class axlAbstractData;

class axlLineAnimationProcessPrivate;

class ANIMATIONTOOLSPLUGIN_EXPORT axlLineAnimationProcess: public axlAbstractProcess
{
    Q_OBJECT
    
public:
    axlLineAnimationProcess(void);
    virtual ~axlLineAnimationProcess(void);
    
    virtual QString description(void) const;
    virtual QString identifier(void) const;
    virtual QString form(void) const;
    
public:
    static bool registered(void);
    
public:
    dtkAbstractData *output(void);
    static QVariant PointInterpolator(const axlPoint &start, const axlPoint &end, qreal progress);
    static QVariant LineInterpolator(const axlLine &start, const axlLine &end, qreal progress);
    static QVariant QvarInterpolator(const QVariantList &start, const QVariantList &end, qreal progress);

public slots:
    void setInput(dtkAbstractData *data, int channel=0);
    int update(void);
    
//public slots:
//    int update(void);
    
private:
    axlLineAnimationProcessPrivate *d;
    
    QPropertyAnimation *animation;
};

dtkAbstractProcess *create_animationTools_axlLineAnimationProcess(void);


