/***********************************************************************
 * animationToolsPlugin.cpp
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: man-c
 *  
 *   
 * Date: Mon Nov  6 17:34:49 2017
 * Changelog: 
 *
 ***********************************************************************/
#include "animationToolsPlugin.h" 
/* <header> */
#include "axlLineAnimationProcess.h"
#include "axlSphereAnimationProcess.h"
#include "axlLineAnimationProcess2.h"
#include "axlSphereSelInputAnimationProcess.h"
/* </header> */

#include <axlGui/axlInspectorObjectFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkLog/dtkLog.h>

// /////////////////////////////////////////////////////////////////
// animationToolsPrivate
// /////////////////////////////////////////////////////////////////

class animationToolsPluginPrivate
{
public:
    // Class variables go here.
};

// /////////////////////////////////////////////////////////////////
// animationTools
// /////////////////////////////////////////////////////////////////

animationToolsPlugin::animationToolsPlugin(QObject *parent) : dtkPlugin(parent), d(new animationToolsPluginPrivate)
{
    
    
}

animationToolsPlugin::~animationToolsPlugin(void)
{
    delete d;
    
    d = NULL;
}

/*!
 * \brief Registers the data, processes and interfaces in their factories.
 */
bool animationToolsPlugin::initialize(void)
{
    dtkWarn()<<"initialize animationToolsPlugin";
    animationToolsPlugin::dataFactSingleton = dtkAbstractDataFactory::instance();
    //dataFactorySingleton();
    animationToolsPlugin::processFactSingleton = dtkAbstractProcessFactory::instance();
    //processFactorySingleton();

    /* <registration> */
    if(!axlLineAnimationProcess::registered()) { dtkWarn() << "Unable to register axlLineAnimationProcess type"; }
    if(!axlSphereAnimationProcess::registered()) { dtkWarn() << "Unable to register axlSphereAnimationProcess type"; }
    if(!axlLineAnimationProcess2::registered()) { dtkWarn() << "Unable to register axlLineAnimationProcess2 type"; }
    if(!axlSphereSelInputAnimationProcess::registered()) { dtkWarn() << "Unable to register axlSphereSelInputAnimationProcess type"; }
    /* </registration> */

    return true;
}

bool animationToolsPlugin::uninitialize(void)
{
    return true;
}

/*!
 * \brief name of the plugin
 * \return a string, which is the name of the plugin
 */
QString animationToolsPlugin::name(void) const
{
    return "animationToolsPlugin";
}
/*!
 * \brief The description of the plugin.
 */
QString animationToolsPlugin::description(void) const
{
    return "a description of my plugin";
}

QStringList animationToolsPlugin::tags(void) const
{
  return QStringList()
    << "Process";
}

/*!
 * \brief List of type names of data, processes, interfaces that the plugin provides.
 * \return a list of strings containing the types handled by the plugin.
 */
QStringList animationToolsPlugin::types(void) const
{
    return QStringList()
    /* <types> */
        << "axlLineAnimationProcess"
        << "axlSereAnimationProcess"
        << "axlLineAnimationProcess2"
        << "axlSereSelInputAnimationProcess"
    /* </types> */
    ;

}

dtkAbstractDataFactory *animationToolsPlugin::dataFactSingleton = NULL;
dtkAbstractProcessFactory *animationToolsPlugin::processFactSingleton = NULL;


