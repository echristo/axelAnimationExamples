///////////////////////////////////////////////////////////////////
// Generated by axel-plugin wizard
///////////////////////////////////////////////////////////////////
/* (C) MyCompany */
/* Put a short description of your plugin here */
/* man-c@mycompany.com http://www.mycompany.com */

#include "axlLineAnimationProcess2.h"
#include "animationToolsPlugin.h"

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlSphere.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlLine.h>
#include <QColor>

#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

//Include from my package
//#include <mypkg/my_class.h>

// /////////////////////////////////////////////////////////////////
// axlLineAnimationProcess2Private
// /////////////////////////////////////////////////////////////////

class axlLineAnimationProcess2Private
{
public:
    QPair<dtkAbstractData *, dtkAbstractData *> input;

    axlAbstractData *output;
};

// /////////////////////////////////////////////////////////////////
// axlLineAnimationProcess2
// /////////////////////////////////////////////////////////////////

axlLineAnimationProcess2::axlLineAnimationProcess2(void) : axlAbstractProcess(), d(new axlLineAnimationProcess2Private)
{
    d->output = NULL;
}

axlLineAnimationProcess2::~axlLineAnimationProcess2(void)
{
    delete d;
    d = NULL;
}

void axlLineAnimationProcess2::setInput(dtkAbstractData *data, int channel)
{
    if(channel==0)
        d->input.first = data;
    else if(channel==1)
        d->input.second = data;
    else
        qDebug()<<"Only two channel available : 0 or 1";
}

dtkAbstractData *axlLineAnimationProcess2::output(void)
{
    return d->output;
}

QVariant axlLineAnimationProcess2::LineInterpolator(const axlLine &start, const axlLine &end, qreal progress) {
//    qDebug() << "\t\tQVariant LineInterpolator: " << progress;
    if (progress < 1.0){
        start.firstPoint()->y() = (end.firstPoint()->y() - start.firstPoint()->y()) * progress + start.firstPoint()->y();
        start.secondPoint()->y() = (end.secondPoint()->y() - start.secondPoint()->y()) * progress + start.secondPoint()->y();
        return QVariant::fromValue(start);
    }
    else
        return QVariant::fromValue(end);
}

int axlLineAnimationProcess2::update(void)
{
    qRegisterAnimationInterpolator<axlLine>(LineInterpolator);

    // move line example
    qRegisterAnimationInterpolator<axlLine>(LineInterpolator);
    axlPoint *point1 = new axlPoint(0.0, 1.0, 0.0);
    axlPoint *point2 = new axlPoint(0, 0.0, 0.0);
    axlLine *line1 = new axlLine(point1, point2);
    line1->setSize(0.1);
    line1->setColor(QColor("blue"));

    animation = new QPropertyAnimation(line1, "line");
    animation->setDuration(5000);
    animation->setStartValue(QVariant::fromValue(axlLine(axlPoint(1.0, 0.0, 0.0),axlPoint(2.0, 0.0, 0.0))));
    animation->setEndValue(QVariant::fromValue(axlLine(axlPoint(1.0, 2.0, 0.0),axlPoint(2.0, 2.0, 0.0))));
    // animation->setKeyValueAt(0, QVariant::fromValue(axlLine(axlPoint(1.0, 1.0, 0.0),axlPoint(2.0, 1.0, 0.0))));
    // animation->setKeyValueAt(0.25, QVariant::fromValue(axlLine(axlPoint(1.0, 2.0, 0.0),axlPoint(2.0, 2.0, 0.0))));
    // animation->setKeyValueAt(0.5, QVariant::fromValue(axlLine(axlPoint(1.0, 3.0, 0.0),axlPoint(2.0, 3.0, 0.0))));
    // animation->setKeyValueAt(0.75, QVariant::fromValue(axlLine(axlPoint(1.0, 4.0, 0.0),axlPoint(2.0, 4.0, 0.0))));
    // animation->setKeyValueAt(1, QVariant::fromValue(axlLine(axlPoint(1.0, 5.0, 0.0),axlPoint(2.0, 5.0, 0.0))));

    connect(animation, SIGNAL(valueChanged(QVariant)), this, SIGNAL(modifiedGeometry()));
    //----------------------------------

    animation->start(QAbstractAnimation::DeleteWhenStopped);

    // Set the output of the process;
    d->output=line1;
    return 1;
}


QString  axlLineAnimationProcess2::form(void) const
{
    return QString(
                " OUTPUT:0 data Out \n");
}

bool axlLineAnimationProcess2::registered(void)
{
    return animationToolsPlugin::processFactSingleton->registerProcessType("axlLineAnimationProcess2", //The identifier of the process
                                                              create_animationTools_axlLineAnimationProcess2, // The constructor function
                                                              "Process" // The tag which appears in the tool cloud.
                                                              );
}

QString axlLineAnimationProcess2::description(void) const
{
    return "MyProc description";
}

QString axlLineAnimationProcess2::identifier(void) const
{
    return "axlLineAnimationProcess2";
}

////////////////////////////////////////////////////////////////////
// Type instanciation
////////////////////////////////////////////////////////////////////

dtkAbstractProcess *create_animationTools_axlLineAnimationProcess2(void)
{
    return new axlLineAnimationProcess2;
}

