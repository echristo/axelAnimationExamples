/***********************************************************************
 * animationToolsPlugin.h
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: man-c
 *  
 *   
 * Date: Mon Nov  6 17:34:49 2017
 * Changelog: 
 *
 ***********************************************************************/

#pragma once

#include <dtkCoreSupport/dtkPlugin.h>

#include "animationToolsExport.h"

class dtkAbstractDataFactory;
class dtkAbstractProcessFactory;

class animationToolsPluginPrivate;

class ANIMATIONTOOLSPLUGIN_EXPORT animationToolsPlugin : public dtkPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.animationToolsPlugin" FILE "animationToolsPlugin.json")

public: 
    animationToolsPlugin(QObject *parent = 0);
    ~animationToolsPlugin(void);
    
    virtual bool initialize(void);
    virtual bool uninitialize(void);
    
    virtual QString name(void) const;
    virtual QString description(void) const;
    
    virtual QStringList tags(void) const;
    virtual QStringList types(void) const;
    
public:
    static dtkAbstractDataFactory *dataFactSingleton;
    static dtkAbstractProcessFactory *processFactSingleton;
    
    
private:
    animationToolsPluginPrivate *d;
};

